#Readme
This is some configuration for the Z-Shell and the gnome-shell to look better and integrate nicely.


##Sources:
This project uses parts of ze-best-zsh-config  
(Github-Site: https://github.com/spicycode/ze-best-zsh-config)

Additionally it adds a hook to notify when long-running-tasks finish and a command-not-found-handler.

##Used Tools and Libaries:
* ze-best-zsh-config: 
https://github.com/spicycode/ze-best-zsh-config
* antigen: https://github.com/zsh-users/antigen
* powerlevel9k-theme:https://github.com/bhilburn/powerlevel9k
* Fira Code as Terminal Font: https://github.com/tonsky/FiraCode
* Plugins from oh-my-zsh: https://github.com/robbyrussell/oh-my-zsh/

