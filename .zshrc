source ~/.zsh/setopt.zsh
source ~/.zsh/completion.zsh
source ~/.zsh/functions.zsh
source ~/.zsh/history.zsh
source ~/.zsh/antigen_bundles.zsh

#disable username in prompt
DEFAULT_USER=$USER
