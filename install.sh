#Install Fira-Code as Powerline-Font
echo "DOWNLOADING Fira-Code Font"
wget -O ~/.local/share/fonts/FiraCode-Regular.otf https://github.com/tonsky/FiraCode/blob/master/FiraCode-Regular.otf?raw=true
echo "Moving Config to ~/.zsh and .zshrc"
cp -r ./.zsh ~/
cp ./.zshrc ~/
#Done
echo "DONE: now use chsh -s $(which zsh) to use zsh as default"
