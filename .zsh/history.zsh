# From ze-best-zsh-config
# Original Source: https://github.com/spicycode/ze-best-zsh-config

# HISTORY
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.zsh_history

bindkey '^R' zaw-history
