source ~/.zsh/tools/antigen.zsh
antigen bundle git
antigen bundle svn
antigen bundle ruby
antigen bundle bundler
antigen bundle rails
antigen bundle gem
antigen bundle gpg-agent
antigen bundle sudo
antigen bundle systemd
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle command-not-found
antigen bundle colorize
antigen bundle cp
antigen theme bhilburn/powerlevel9k powerlevel9k
POWERLEVEL9K_MODE='awesome-patched'
antigen apply
